package com.techu.entregables.entregable2.v1.datos;

import com.techu.entregables.entregable2.v1.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String> {

  }
