package com.techu.entregables.entregable2.v1.controlador;

import com.techu.entregables.entregable2.v1.modelo.Usuario;
import com.techu.entregables.entregable2.v1.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v2/productos/{idProducto}/usuarios")
//@RequestMapping("${api.version}/{idProducto}/usuarios")

public class ControladorUsuariosProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public List<Usuario> obtenerUsuarios(@PathVariable String idProducto) {
        try {
            return this.servicioProducto.obtenerUsuariosProducto((idProducto));
        } catch (IllegalArgumentException x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getLocalizedMessage());
        }
    }
}
