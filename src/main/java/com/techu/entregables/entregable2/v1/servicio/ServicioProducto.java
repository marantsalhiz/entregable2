package com.techu.entregables.entregable2.v1.servicio;

import com.techu.entregables.entregable2.v1.modelo.Producto;
import com.techu.entregables.entregable2.v1.modelo.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioProducto {
    //CRUD

    //POST /productos
    public Producto crearProducto(Producto producto);

    //GET /productos
    public List<Producto> obtenerProductos();

    //GET /productos/{id}
    public Producto obtenerProductoPorId(String id);



    //PUT /productos/{id} @RequestBody
    public Producto reemplazarProducto(String id, Producto productoNuevo);



    //DELETE /productos/{id}
    public void borrarProductoPorId(String id);

    //GET /productos/{idProducto}/usuario
    public List<Usuario> obtenerUsuariosProducto(String idProducto);


}
