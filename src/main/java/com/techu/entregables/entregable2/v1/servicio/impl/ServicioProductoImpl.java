package com.techu.entregables.entregable2.v1.servicio.impl;

import com.techu.entregables.entregable2.v1.datos.RepositorioProducto;
import com.techu.entregables.entregable2.v1.modelo.Producto;
import com.techu.entregables.entregable2.v1.modelo.Usuario;
import com.techu.entregables.entregable2.v1.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    public ServicioProductoImpl() {
        System.out.println("============== ServicioProductoImpl");
    }

    @Autowired
    RepositorioProducto productoRepository;

    @Override
    public Producto crearProducto(Producto producto) {

        return this.productoRepository.insert(producto);
    }

    @Override
    public List<Producto> obtenerProductos() {
        return this.productoRepository.findAll();
    }

    @Override
    public Producto obtenerProductoPorId(String id) {
        final Optional<Producto> p = this.productoRepository.findById(id);
        return p.isPresent() ? p.get() : null;
    }



    @Override
    public Producto reemplazarProducto(String id, Producto productoNuevo) {
        final Optional<Producto> p = this.productoRepository.findById(id);
        if(p.isPresent()) {
            productoNuevo.setId(id);
            this.productoRepository.save(productoNuevo);
            return productoNuevo;
        }
        return null;
    }




    @Override
    public void borrarProductoPorId(String id) {
        this.productoRepository.deleteById(id);
    }

    @Override
    public List<Usuario> obtenerUsuariosProducto(String idProducto) {
        final Producto p = this.obtenerProductoPorId(idProducto);
        if(p == null)
            throw new IllegalArgumentException("No existe el producto");
        final List<Usuario> usuarios = p.getUsuarios();
        return usuarios == null
                ? Collections.emptyList()
                : usuarios
                ;
    }



}
